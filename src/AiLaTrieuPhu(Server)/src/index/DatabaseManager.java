package index;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.*;

public class DatabaseManager {
    private String DATABASE_PATH = "jdbc:sqlite:C:/sqlite/Question.db";
    Connection conn = null;
    Statement stmt = null;

    private static final String SQL_GET_15_QUESTION = "select * from (select* from Question order by random()) group by level order by level limit 15";


    public DatabaseManager() {}
    
    private void openDatabase() {
    	try {
            conn = DriverManager.getConnection(DATABASE_PATH);
         } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
         }
    }

    private void closeDatabase() {
    	try {  
            if (conn != null) {  
                conn.close();  
            }  
        } catch (SQLException ex) {  
            System.out.println(ex.getMessage());  
        } 
    }
    
    public ArrayList<Question> get15Questions() {
        openDatabase();
        ArrayList<Question> questions = new ArrayList<>();
        
        try {
        	stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SQL_GET_15_QUESTION);

            while ( rs.next() ) {
                String q = rs.getString("question");
                String a = rs.getString("casea");
                String b = rs.getString("caseb");
                String c = rs.getString("casec");
                String d = rs.getString("cased");
                int t = rs.getInt("truecase");
                int l = rs.getInt("level");
                questions.add(new Question(q, a, b, c, d, t, l));
            }
            rs.close();
            stmt.close();
		} catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		}
        
        closeDatabase();
        return questions;
    }
    
    public Question getQuestionByLevel(int lv) {
        openDatabase();
        String query = "select * from Question where level = '" + lv + "' order by random() limit 1";
        Question ques = null;
        
        try {
        	stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            String question, caseA, caseB, caseC, caseD;
            int level, trueCase;

            question = rs.getString("question");
            caseA = rs.getString("casea");
            caseB = rs.getString("caseb");
            caseC = rs.getString("casec");
            caseD = rs.getString("cased");
            level = rs.getInt("level");
            trueCase = rs.getInt("truecase");

            ques = new Question(question, caseA, caseB, caseC, caseD, trueCase, level);
        	
        } catch (Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		}
        closeDatabase();
        return ques;
    }

    public ArrayList<HighScore> getHighScore() {
        openDatabase();
        String query = "select * from HighScore ORDER BY Score DESC";
        ArrayList<HighScore> highScores = new ArrayList<>();

        try {
        	stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query); 
            
            if (rs == null) {
            	return null;
            }
            
            while(rs.next()) {
            	String name = rs.getString("Name");
                int score = rs.getInt("Score");

                highScores.add(new HighScore(name, score));
            }
            
        } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		}
        closeDatabase();
        return highScores;
    }

    public void insertHighScore(String name, int score) {
    	openDatabase();
    	String query = "insert into HighScore(Name, Score) values(?, ?)";
    	try {
    		PreparedStatement pstmt = conn.prepareStatement(query);
    		pstmt.setString(1, name);
    		pstmt.setInt(2, score);
            pstmt.executeUpdate();
    	} catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		}
    	
    	closeDatabase();
    }
}
