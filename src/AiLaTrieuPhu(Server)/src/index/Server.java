package index;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import com.google.gson.*;

import model.HighScore;
import model.Question;


public class Server {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Server();
	}

	public Server() {
		int port = 5000;
		

		try {
			InetAddress myHost = InetAddress.getLocalHost();
			System.out.println(myHost.getHostAddress());
			System.out.println(myHost.getHostName());
			ServerSocket ss = new ServerSocket(port);
			while (true) {
				Socket s = ss.accept();
				Handle h1 = new Handle(s, this);
				h1.start();
				System.out.println("Start server!");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

class Handle extends Thread {
	Socket socket;
	Server server;
	InputStream is;
	DataInputStream dis;
	OutputStream os;
	OutputStreamWriter osw;
	BufferedWriter bw;

	DatabaseManager db = new DatabaseManager();
	Gson gson = new Gson();

	public Handle(Socket socket, Server server) {
		this.socket = socket;
		this.server = server;
		try {
			is = socket.getInputStream();
			dis = new DataInputStream(is);

			os = socket.getOutputStream();
			osw = new OutputStreamWriter(os, "UTF-8");
			bw = new BufferedWriter(osw);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			boolean log = true;
			while (log) {
				// READ
				String res = dis.readUTF();
				String[] command = new String[res.length()];
				if (res.contains(", "))
					command = res.split(", ");
				else
					command[0] = res;

				switch (command[0]) {
				case "connect":
					System.out.println("Connected");
					connect(socket);
					break;
				case "get15Questions":
					get15Questions(socket);
					break;
				case "getHighScore":
					getHighScore(socket);
					break;
				case "changeQuestion":
					int level = Integer.parseInt(command[1]);
					changeQuestion(socket, level);
					break;
				case "saveScore":
					String name = command[1];
					int score = Integer.parseInt(command[2]);
					db.insertHighScore(name, score);
					break;
				case "finishApp":
					finishApp(socket);
					log = false;
					System.out.println("Closed server!");
					break;

				default:
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Trả về client connect thành công tới server
	private void connect(Socket s) {
		try {
			String response = "connected\n";
			bw.write(response);
			bw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Trả về client câu hỏi ở mức level client yêu cầu
	private void changeQuestion(Socket s, int lv) {
		try {
			Question ques = db.getQuestionByLevel(lv);
			String response = gson.toJson(ques);
			response += "\n";
			System.out.println(response);
			bw.write(response);
			bw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Trả về client bộ 15 câu hỏi
	private void get15Questions(Socket socket) {
		try {
			ArrayList<Question> al = db.get15Questions();
			String response = gson.toJson(al);
			response += "\n";
			System.out.println(response);
			bw.write(response);
			bw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Lấy lịch sử điểm cao của người chơi trả về client
	private void getHighScore(Socket s) {
		try {
			// WRITE
			ArrayList<HighScore> al = db.getHighScore();
			String response = gson.toJson(al);
			response += "\n";
			System.out.println(response);
			bw.write(response);
			bw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Gửi thông báo đã đóng server tới client
	private void finishApp(Socket s) {
		try {
			String response = "closed\n";
			bw.write(response);
			bw.flush();
			
			os.close();
			osw.close();
			bw.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
