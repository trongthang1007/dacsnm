package thangtt.ailatrieuphu.Async;
/*
* config connect server
* */
public class Config {
    private static String IP;
    private static final int PORT = 5000;

    public static int getPORT() {
        return PORT;
    }

    public static String getIP() {
        return IP;
    }

    public Config(String ip) {
        this.IP = ip;
    }
}
