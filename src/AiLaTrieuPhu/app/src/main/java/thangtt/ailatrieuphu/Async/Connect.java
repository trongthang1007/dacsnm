package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/*
 * Tạo 1 luồng mới để connect tới server
 * */

public class Connect extends AsyncTask<Void, Void, Void> {
    private Config config;
    private OutputStream os;
    private DataOutputStream dos;
    private InputStream is;
    private InputStreamReader isr;
    private BufferedReader br;
    private Socket s;
    private PrintWriter pw = null;
    private static String respond = "";
    private Gson gson = new Gson();

    public Connect(String ip) {
        config = new Config(ip);
    }

    public DataOutputStream getDos() {
        return dos;
    }

    public BufferedReader getBr() {
        return br;
    }

    public PrintWriter getPw() {
        return pw;
    }

    public void closeConnect() {
        try {
            os.close();
            dos.close();
            is.close();
            isr.close();
            br.close();
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d("TESST LOIOXII:", "LỖI HOÀI");

        try {
            s = new Socket(config.getIP(), config.getPORT());
            os = s.getOutputStream();
            dos = new DataOutputStream(os);

            is = s.getInputStream();
            isr = new InputStreamReader(is, "UTF-8");
            br = new BufferedReader(isr);

            if (pw == null) {
                pw = new PrintWriter(s.getOutputStream());
            }

            String command = "connect";


            dos.writeUTF(command);
            dos.flush();

            String message = br.readLine();
            respond = gson.fromJson(message, new TypeToken<String>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String getRespond() {
        return respond;
    }
}
