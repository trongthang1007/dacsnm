package thangtt.ailatrieuphu.Async;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import thangtt.ailatrieuphu.activity.MainActivity;

public class StickyService extends Service {

    private MainActivity mainActivity;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mainActivity = new MainActivity();
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        mainActivity.closeApp();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}