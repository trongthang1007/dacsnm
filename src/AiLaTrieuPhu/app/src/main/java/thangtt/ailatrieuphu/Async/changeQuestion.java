package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import thangtt.ailatrieuphu.dialog.IpDialog;
import thangtt.ailatrieuphu.model.Question;

/*
* Tạo 1 luồng mới để connect tới server --> trợ giúp đổi câu hỏi
* */

public class changeQuestion extends AsyncTask<Void, Void, Void> {
    private Question question;
    private Gson gson = new Gson();
    private int level;

    public changeQuestion(int level) {
        this.level = level;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String command = "changeQuestion, " + this.level;
            IpDialog.getConnect().getDos().writeUTF(command);
            IpDialog.getConnect().getDos().flush();

            String message = IpDialog.getConnect().getBr().readLine();
            Log.d("test", message);
            question = gson.fromJson(message, new TypeToken<Question>(){}.getType());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public Question getQuestion() {
        return question;
    }
}
