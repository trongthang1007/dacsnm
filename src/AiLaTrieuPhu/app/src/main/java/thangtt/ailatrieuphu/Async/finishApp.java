package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import thangtt.ailatrieuphu.dialog.IpDialog;

public class finishApp extends AsyncTask {

    private String respond;
    private Gson gson = new Gson();
    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            String command = "finishApp";
            IpDialog.getConnect().getDos().writeUTF(command);
            IpDialog.getConnect().getDos().flush();

            String message = IpDialog.getConnect().getBr().readLine();
            respond = gson.fromJson(message, new TypeToken<String>(){}.getType());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public String getRespond() {
        return respond;
    }
}
