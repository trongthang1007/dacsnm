package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import thangtt.ailatrieuphu.dialog.IpDialog;
import thangtt.ailatrieuphu.model.Question;

/*
 * Tạo 1 luồng mới để connect tới server --> khi bắt đầu
 * chơi sẽ lấy bộ 15 câu hỏi
 * */

public class get15Questions extends AsyncTask<Void, Void, Void> {

    private ArrayList<Question> questions;
    private Gson gson = new Gson();

    public get15Questions() {
        questions = new ArrayList<>();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String command = "get15Questions";
            IpDialog.getConnect().getDos().writeUTF(command);
            IpDialog.getConnect().getDos().flush();

            String message = IpDialog.getConnect().getBr().readLine();
            questions = gson.fromJson(message, new TypeToken<ArrayList<Question>>(){}.getType());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }
}