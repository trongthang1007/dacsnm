package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import thangtt.ailatrieuphu.dialog.IpDialog;
import thangtt.ailatrieuphu.model.HighScore;

/*
 * Tạo 1 luồng mới để connect tới server --> lấy điểm cao của người chơi
 * đã lưu điểm lại
 * */

public class getHighScore extends AsyncTask<Void, Void, Void> {
    private ArrayList<HighScore> highScores;
    private Gson gson = new Gson();

    public getHighScore() {
        highScores = new ArrayList<>();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String command = "getHighScore";
            IpDialog.getConnect().getDos().writeUTF(command);
            IpDialog.getConnect().getDos().flush();

            String message = IpDialog.getConnect().getBr().readLine();
            highScores = gson.fromJson(message, new TypeToken<ArrayList<HighScore>>(){}.getType());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public ArrayList<HighScore> getHighScores() {
        return highScores;
    }
}
