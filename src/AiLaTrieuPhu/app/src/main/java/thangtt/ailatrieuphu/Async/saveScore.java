package thangtt.ailatrieuphu.Async;

import android.os.AsyncTask;

import thangtt.ailatrieuphu.dialog.IpDialog;
import thangtt.ailatrieuphu.fragments.HomeFragment;

/*
 * Tạo 1 luồng mới để connect tới server --> khi chơi xong sẽ lưu điểm
 * */

public class saveScore extends AsyncTask<Void, Void, Void> {
    private String name;
    private int score;

    public saveScore(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String command = "saveScore, " + this.name + ", " + this.score;
            IpDialog.getConnect().getDos().writeUTF(command);
            IpDialog.getConnect().getDos().flush();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
