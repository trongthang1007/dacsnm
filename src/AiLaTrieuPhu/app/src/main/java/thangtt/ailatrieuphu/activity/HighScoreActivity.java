package thangtt.ailatrieuphu.activity;

import android.app.ListActivity;
import android.os.Bundle;

import thangtt.ailatrieuphu.Async.getHighScore;
import thangtt.ailatrieuphu.R;
import thangtt.ailatrieuphu.adapter.HighScoreAdapter;
import thangtt.ailatrieuphu.manager.MusicManager;

public class HighScoreActivity extends ListActivity {
    private MusicManager musicManager;
    private getHighScore highScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        musicManager = new MusicManager(this);

        highScore = new getHighScore();
        highScore.execute();
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getListView().setAdapter(new HighScoreAdapter(this, highScore.getHighScores()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        musicManager.pauseBgMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        musicManager.resumeBgMusic();
    }
}
