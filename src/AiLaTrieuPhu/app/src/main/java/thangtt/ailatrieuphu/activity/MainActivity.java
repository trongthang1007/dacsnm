package thangtt.ailatrieuphu.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import thangtt.ailatrieuphu.Async.StickyService;
import thangtt.ailatrieuphu.Async.finishApp;
import thangtt.ailatrieuphu.R;
import thangtt.ailatrieuphu.dialog.IpDialog;
import thangtt.ailatrieuphu.dialog.NoticeDialog;
import thangtt.ailatrieuphu.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private finishApp finishApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent stickyService = new Intent(this, StickyService.class);
        startService(stickyService);
        initComponents();
    }

    //Animation chờ 3s show trang HomeFragment
    private void initComponents() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.bg_circle_rotate);
        animation.setDuration(3000);
        findViewById(R.id.load).startAnimation(animation);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_main, new HomeFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        final NoticeDialog noticeDialog = new NoticeDialog(this);
        noticeDialog.setCancelable(true);
        noticeDialog.setNotification("Bạn có muốn thoát khỏi trò chơi", "Đồng ý", "Hủy", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.btn_ok) {
                    closeApp();
                    finish();
                }
                noticeDialog.dismiss();
            }
        });
        noticeDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void closeApp() {
        finishApp = new finishApp();
        finishApp.execute();
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (finishApp.getRespond().equals("closed")) {
            IpDialog.getConnect().closeConnect();
        }
    }
}
