package thangtt.ailatrieuphu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import thangtt.ailatrieuphu.Async.Connect;
import thangtt.ailatrieuphu.R;

public class IpDialog extends Dialog {

    private static Connect connect;
    private EditText edtIp;
    private Button btnOk;
    private NoticeDialog noticeDialog;

    public IpDialog(final Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ip_dialog);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        noticeDialog = new NoticeDialog(getContext());
        edtIp = findViewById(R.id.edt_ip);
        btnOk = findViewById(R.id.btn_ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIp.getText().toString().isEmpty()) {
                    dialogNotice("Phải nhập địa chỉ IP để connect tới server!");
                    return;
                }
                connect = new Connect(edtIp.getText().toString());
                connect.executeOnExecutor(connect.THREAD_POOL_EXECUTOR);
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(connect.getRespond().equals("connected")) {
                    dialogNotice("Kết nối tới server thành công!");
                    connect.cancel(true);
                    dismiss();
                } else {
                    dialogNotice("Kết nối tới server thất bại!");
                    connect.cancel(true);
                }
            }
        });
    }

    private void dialogNotice(String str) {
        noticeDialog.setNotification(str, "Đóng", null, null);
        noticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        noticeDialog.show();
    }

    public static Connect getConnect() {
        return connect;
    }
}
