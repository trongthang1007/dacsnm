package thangtt.ailatrieuphu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import thangtt.ailatrieuphu.Async.saveScore;
import thangtt.ailatrieuphu.R;

public class ScoreDialog extends Dialog {

    private int score;
    private EditText edtName;
    private TextView tvScore;
    private Button btnOk;
    private saveScore saveScore;

    public ScoreDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.score_dialog);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        edtName = findViewById(R.id.edt_name);
        tvScore = findViewById(R.id.tv_score);
        btnOk = findViewById(R.id.btn_ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtName.getText().toString().isEmpty()) {
                    return;
                }
                saveScore = new saveScore(edtName.getText().toString().trim(), score);
                saveScore.execute();
                dismiss();
            }
        });
    }

    public void getScore(String score) {
        tvScore.setText(score + "VNĐ");
        this.score = Integer.parseInt(score.replaceAll(",", ""));
    }

    public void setScore(String score) {
        tvScore.setText(score + " VNĐ");
        this.score = Integer.parseInt(score.replaceAll(",", ""));
    }
}
