import java.util.Random;

/*
 * công việc của Consumer là tiêu thụ dữ liệu (nghĩa là loại bỏ nó khỏi bộ đệm), 
 * từng phần một và xử lý nó. Consumer và Producer hoạt động song song với nhau.
 */

public class Consumer extends Thread{
	private Repository rep;
	private boolean isRunning = true;
	private int name;
	private int sleepTime;
	
	public Consumer(Repository rep, int name, int sleepTime) {
		this.rep = rep;
		this.name = name;
		this.sleepTime = sleepTime;
	}
	
	public void pause() {
		isRunning = false;
	}
	
	@Override
	public void run() {
		while(isRunning) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Random rnd = new Random();
			int n = rnd.nextInt(rep.getSize() / 2) + 1;
			rep.get(n, name);
		}
	}
}
