import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

public class Index {

	private JFrame frame;
	private JButton btnStart, btnStop, btnClear;
	private TextArea txtArea;
	private JProgressBar repDemo;
	private JLabel lbSleepTime, lbSize, lbRep;
	private JTextField txtSleepTime, txtSize;
	
	private static Producer p1, p2;
	private static Consumer c1, c2;
	private static Repository rep;
	private static int sleepTime, size;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					Index window = new Index();
					window.frame.setBounds(700, 350, 700, 420);
					window.frame.setVisible(true);
					window.frame.setTitle("Producer-Consumer Demo");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Index() {
		initUI();
	}
	
	//Khởi tạo các thành phần giao diện
	private void initUI() {
		frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		lbSleepTime = new JLabel("Sleep Time");
		txtSleepTime = new JTextField();
		txtSleepTime.setText("2000");
		txtSleepTime.setColumns(10);
		
		lbSize = new JLabel("Size");
		txtSize = new JTextField();
		txtSize.setText("10");
		txtSize.setColumns(10);
		
		btnStart = new JButton("Start");
		btnStop = new JButton("Stop");
		btnClear = new JButton("Clear");
		
		txtArea = new TextArea(10,58);
		txtArea.setBackground(Color.decode("#ffffff"));
		txtArea.setForeground(Color.BLACK);
		txtArea.setFont(new Font("SansSerif", Font.PLAIN, 20));
		
		lbRep = new JLabel ("Kho chứa: ");
		repDemo = new JProgressBar(0, 100);
		repDemo.setValue(0);
		repDemo.setMinimum(0);

		frame.getContentPane().add(lbSize);
		frame.getContentPane().add(txtSize);
		frame.getContentPane().add(lbSleepTime);
		frame.getContentPane().add(txtSleepTime);
		frame.getContentPane().add(btnStart);
		frame.getContentPane().add(btnStop);
		frame.getContentPane().add(btnClear);
		frame.getContentPane().add(txtArea);
		frame.getContentPane().add(lbRep);
		frame.getContentPane().add(repDemo);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addListener();
	}
	
	//Viết hành vi cho các sự kiện
	private void addListener() {
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					sleepTime = Integer.parseInt(txtSleepTime.getText());
					size = Integer.parseInt(txtSize.getText());
					if(sleepTime < 0 && size < 0) {
						throw new Exception("Nhập số lớn hơn 0!");
					}
					repDemo.setMaximum(size);
					txtArea.setText("");
					txtSize.setText(String.valueOf(size));
					start();
				} catch (NumberFormatException e2) {
					showDialog("Vui lòng nhập thông tin còn thiếu!");
				}catch (Exception e2) {
					showDialog(e2.getMessage());
				}
			}
		});
		btnStop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stop(p1);
				stop(p2);
				stop(c1);
				stop(c2);
			}
		});
		btnClear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				txtArea.setText("");
			}
		});
	}
	
	//Show thông báo khi nhập dữ liệu vào bị sai
	public void showDialog(String msg) {
		JOptionPane.showMessageDialog(null,msg,"Thông báo", JOptionPane.WARNING_MESSAGE);
	}

	//Dừng các Producer và Consumer
	public void stop(Thread t) {
		p1.pause();
		p2.pause();
		c1.pause();
		c2.pause();
	}
	
	//Thêm text vào TextArea để mô phỏng kết quả
	public void appendText(String str) {
		txtArea.append(str);
	}
	
	//Thay đổi hình ảnh mô phỏng của kho chứa (xem hàng trong kho còn bao nhiêu)
	public void setChangeProgress(int number) {
		repDemo.setValue(number);
		repDemo.setStringPainted(true);
	}
	
	private void start() {
		// TODO Auto-generated method stub
		rep = new Repository(size, this);
		p1 = new Producer(rep, 1, sleepTime);
		c1 = new Consumer(rep, 1, sleepTime);
		p2 = new Producer(rep, 2, sleepTime);
		c2 = new Consumer(rep, 2, sleepTime);
		
		p1.start();
		p2.start();
		c1.start();
		c2.start();
	}
}
