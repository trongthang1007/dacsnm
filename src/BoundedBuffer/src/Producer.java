import java.util.Random;

//công việc của Producer là tạo dữ liệu, đưa nó vào bộ đệm và bắt đầu lại.
public class Producer extends Thread{
	private Repository rep;
	private boolean isRunning = true;
	private int name;
	private int sleepTime;
	
	public Producer(Repository rep, int name, int sleepTime) {
		this.rep = rep;
		this.name = name;
		this.sleepTime = sleepTime;
	}
	
	public void pause() {
		isRunning = false;
	}
	
	@Override
	public void run() {
		while(isRunning) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Random rnd = new Random();
			int n = rnd.nextInt(rep.getSize() / 2) + 1;
			rep.add(n, name); 
		}
	}
}
