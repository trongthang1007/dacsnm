import java.util.LinkedList;


//BLOCKING QUEUE sử dụng synchronized để đảm bảo thread-safe
public class Repository {
	private int max;
	private LinkedList<Object> queue = new LinkedList<>();
	private Index jframe;
	
	public Repository(int max, Index index) {
		this.max = max;
		jframe = index;
	}
	
	//số lượng tối đa trong kho
	public int getSize() {
		return max;
	}
	
	//xem số lượng hàng trong kho
	public int getQuanlityOfInventory() {
		return queue.size();
	}
	
	
	//check kho chứa đã full chưa
	public boolean isFull() {
		return getQuanlityOfInventory() == max;
	}
	
	//thêm vào kho chứa sử dụng synchronized để đồng bộ phương thức add để tránh xảy ra xung đột
	public synchronized void add(int quanlity, int name) {
		try {
			while(quanlity > max - getQuanlityOfInventory()) {
				jframe.appendText("Producer "+name+" đang chờ nhập "+quanlity+" sản phẩm\n");
				wait();
			}
			for (int i = 0; i < quanlity; i++) {
				queue.add(1);
			}
			jframe.appendText("Producer "+name+" đã nhập "+quanlity+" sản phẩm vào kho. Kho hiện có: "+getQuanlityOfInventory()+"\n");
			jframe.setChangeProgress(getQuanlityOfInventory());
			notifyAll();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//lấy sản phẩm ra khỏi kho chứa sử dụng synchronized để đồng bộ phương thức get để tránh xảy ra xung đột
	public synchronized void get(int quanlity, int name) {
		try {
			while (quanlity > getQuanlityOfInventory()) {
				jframe.appendText("Consumer "+name+" đang chờ xuất "+quanlity+" sản phẩm\n");
				wait();
			}
			for(int i = 0; i < quanlity; i ++) {
				queue.poll();
			}
			jframe.appendText("Consumer "+name+" đã xuất "+ quanlity+" sản phẩm ra khỏi kho.Kho hiện còn: "+getQuanlityOfInventory()+"\n");
			jframe.setChangeProgress(getQuanlityOfInventory());
			notifyAll();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
